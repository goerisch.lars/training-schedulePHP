<?php

namespace Tests\TrainingScheduleBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class TrainingControllerTest extends WebTestCase
{
    protected static $date;
    protected static $futureDate;
    
    protected static $client;

    /**
     * Sets up dates required for Training form / suggestion testing
     */
    public static function setUpBeforeClass()
    {
        self::$date = date('d-m-Y');
        self::$futureDate = date('d-m-Y', strtotime('+1 week'));
    }

    public function testNewTraining()
    {
        $fixtures = self::loadFixtures(array('\TrainingScheduleBundle\DataFixtures\ORM\LoadUserData'))->getReferenceRepository();
        $this->loginAs($fixtures->getReference('user1'), 'main');
        self::$client = static::makeClient();
        self::$client->followRedirects(true);


        $crawler = self::$client->request('GET', '/training/new/' . self::$date);
        $form = $crawler->selectButton('Eintragen')->form(
            array(
                'training_container[Training][name]'                      => 'Test Strength Training',
                'training_container[Training][note]'                      => 'Making a note here!',
                'training_container[Training][strengthTraining][reruns]'  => '3',
                'training_container[Training][strengthTraining][sets]'    => '10',
                'training_container[Training][strengthTraining][weight]'  => '20',
            )
        );

        self::$client->submit($form);


        $crawler = self::$client->request('GET', '/overview/index');
        $this->assertTrue($crawler->filter('a:contains("Test Strength Training")')->count() == 1, 'User can add training and see it on overview.');
    }

    /**
     * Checks if suggestion is being displayed in the following week
     * @depends testNewTraining
     */
    public function testSuggestions()
    {
        $crawler = self::$client->request('GET', '/training/new/' . self::$futureDate);
        $this->assertEquals(1, $crawler->filterXPath('//div[@id="weekdaysuggestion"]/div/div[contains(text(),\'Test Strength Training\')]')->count(), 'Suggestions created in the week before are displayed to user.');
    }

    /**
     * @depends testSuggestions
     */
    public function testDeletion()
    {
        $crawler = self::$client->request('GET', '/overview/index');
        $this->assertEquals(1,  $crawler->filter('a:contains("Test Strength Training")')->count(), 'Training is present on overview page.');

        //Navigating to deletion URL
        $trainingUrl = $crawler->filter('a:contains("Test Strength Training")')->attr('href');
        $crawler = self::$client->request('GET', $trainingUrl);
        $deleteUrl = $crawler->filter('a:contains("Löschen")')->attr('href');
        self::$client->request('GET', $deleteUrl);

        $crawler = self::$client->request('GET', '/overview/index');
        $this->assertEquals(0,  $crawler->filter('a:contains("Test Strength Training")')->count(), 'Training was deleted.');

    }
}
