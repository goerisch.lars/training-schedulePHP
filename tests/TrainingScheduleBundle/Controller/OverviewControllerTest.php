<?php

namespace Tests\TrainingScheduleBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class OverviewControllerTest extends WebTestCase
{

    public function testView()
    {
        $fixtures = self::loadFixtures(array('\TrainingScheduleBundle\DataFixtures\ORM\LoadUserData'))->getReferenceRepository();
        $this->loginAs($fixtures->getReference('user1'), 'main');
        $client = static::makeClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/overview/index');
        $overviewElems = $crawler->filterXPath('//td[@class="row"]');
        $this->assertEquals(7, $overviewElems->count(), 'User can access overview page and finds a row for each day of the week.');
    }



}
