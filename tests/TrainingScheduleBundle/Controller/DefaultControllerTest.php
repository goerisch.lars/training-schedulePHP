<?php

namespace Tests\TrainingScheduleBundle\Controller;

use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TrainingScheduleBundle\Entity\User;

class DefaultControllerTest extends WebTestCase
{
    private static function deleteTestUser() {
        self::bootKernel();
        /** @var User $user */
        $user = self::getTestUser();
        if ($user) {
            static::$kernel->getContainer()->get('fos_user.user_manager')->deleteUser($user);
        }
    }

    /**
     * @return User|null
     */
    private static function getTestUser() {
        self::bootKernel();
        $um = static::$kernel
            ->getContainer()
            ->get('fos_user.user_manager');

        return $um->findUserBy(array('username' => 'unit_test_user'));
    }

    public static function setUpBeforeClass()
    {
        self::deleteTestUser(); //Making sure the user does not in the DB before tests run
    }

    public function testRegistration()
    {
        $user = self::getTestUser();
        $this->assertNull($user, 'Test user did not exist before test execution.');

        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/register');
        $form = $crawler->selectButton('Registrieren')->form(
            array(
                'fos_user_registration_form[username]'              => 'unit_test_user',
                'fos_user_registration_form[email]'                 => 'unit_test@testing.com',
                'fos_user_registration_form[plainPassword][first]'  => 'testpassword123',
                'fos_user_registration_form[plainPassword][second]' => 'testpassword123',
            )
        );

        $client->submit($form);

        $user = self::getTestUser();
        $this->assertEquals('unit_test_user', $user->getUsername(), 'User was created via registration form.');
    }

    /**
     * @depends testRegistration
     */
    public function testLogin()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/overview/index');
        $registrationURLElem = $crawler->filterXPath('//a[@id="registration_ref"]');
        $this->assertTrue($registrationURLElem->count() === 1, 'Anonymous user was unable to access overview and redirected to login page.');

        $form = $crawler->selectButton('Anmelden')->form(
            array(
                '_username'  => 'unit_test_user',
                '_password'  => 'testpassword123',
            )
        );

        $client->submit($form);
        $crawler = $client->request('GET', '/overview/index');
        $registrationURLElem = $crawler->filterXPath('//h1[@id="overview-headline"]');
        $this->assertTrue($registrationURLElem->count() === 1, 'User can log in and can access weekly overview page.');

    }

    public static function tearDownAfterClass()
    {
        self::deleteTestUser(); //Deleting test user after tests are over
    }
}
