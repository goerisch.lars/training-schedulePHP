set :stages,                %w(staging)
set :default_stage,         "staging"
set :stage_dir,             "app/config/deploy"
require                     'capistrano/ext/multistage'

set :application,           "training_schedule"
set :user,                  "local"
set :use_sudo,              false
set :deploy_to,             "/var/www/schedule"
set :copy_remote_dir,       "/home/local"


set :scm,                   :git
set :repository,            "https://github.com/blackbirdone/training-schedulePHP.git"
set :deploy_via,            :copy
set :composer_bin,          "/usr/local/bin/composer"
set :shared_children,       ["vendor", "var"]

set :model_manager,         "doctrine"
set :use_composer,          true
set :dump_assetic_assets,   true

set  :keep_releases,        3

task :upload_parameters do
  origin_file = "app/config/parameters.yml"
  destination_file = latest_release + "/app/config/parameters.yml" # Notice the
  latest_release
  try_sudo "mkdir -p #{File.dirname(destination_file)}"
  top.upload(origin_file, destination_file)
end
before "deploy:share_childs", "upload_parameters"