<?php

namespace TrainingScheduleBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use TrainingScheduleBundle\Security\AccessAttributes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\SecurityEvents;
use TrainingScheduleBundle\Entity\StrengthTraining;
use TrainingScheduleBundle\Entity\TrainingDay;
use TrainingScheduleBundle\Form\InheritanceType\StrengthTrainingType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class StrengthTrainingController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        return $this->render(
            'TrainingScheduleBundle:StrengthTraining:index.html.twig',
            array(// ...
            )
        );
    }

    /**
     * @Route("/view/{id}")
     * @ParamConverter("strengthTraining", class="TrainingScheduleBundle:StrengthTraining")
     * @param StrengthTraining $strengthTraining
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(StrengthTraining $strengthTraining)
    {
        $this->denyAccessUnlessGranted(AccessAttributes::VIEW, $strengthTraining);

        $form = $this->createForm(
            StrengthTrainingType::class,
            $strengthTraining,
            array(
                'disabled' => true,
            )
        );

        return $this->render(
            'TrainingScheduleBundle:Training:view.html.twig',
            array(
                'form' => $form->createView(),
                'type' => $strengthTraining::__className(),
                'id'   => $strengthTraining->getId()
                // ...
            )
        );
    }

    /**
     * @Route("/edit/{id}")
     * @ParamConverter("strengthTraining", class="TrainingScheduleBundle:StrengthTraining")
     * @param Request          $request
     * @param StrengthTraining $strengthTraining
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, StrengthTraining $strengthTraining)
    {
        $this->denyAccessUnlessGranted(AccessAttributes::VIEW, $strengthTraining);

        $form = $this->createForm(StrengthTrainingType::class, $strengthTraining, array());

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($strengthTraining);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('trainingschedule_overview_index');
            }
        }

        return $this->render(
            'TrainingScheduleBundle:StrengthTraining:edit.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/delete/{id}")
     * @ParamConverter("strengthTraining", class="TrainingScheduleBundle:StrengthTraining")
     * @param StrengthTraining $strengthTraining
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(StrengthTraining $strengthTraining)
    {
        $this->denyAccessUnlessGranted(AccessAttributes::VIEW, $strengthTraining);

        $this->getDoctrine()->getManager()->remove($strengthTraining);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('trainingschedule_overview_index');
    }

    /**
     * @Route("/new")
     */
    public function newAction()
    {
        return $this->render(
            'TrainingScheduleBundle:StrengthTraining:new.html.twig',
            array(// ...
            )
        );
    }

}
