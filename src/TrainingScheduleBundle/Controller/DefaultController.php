<?php

namespace TrainingScheduleBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TrainingScheduleBundle\Form\UserType;
use TrainingScheduleBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class DefaultController
 *
 * @package TrainingScheduleBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('fos_user_security_login'));
    }

    /**
     * @Route("/overview")
     */
    public function overviewAction()
    {
        return $this->redirect($this->generateUrl('trainingschedule_overview_index'));
    }

    /**
     * /**
     * @Route("/lang/{locale}")
     * @param $locale string
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLanguageAction($locale)
    {
        if ($locale === 'de' || $locale === 'en') {
            $this->get('session')->set('_locale', $locale);
        }

        return $this->redirect($this->generateUrl('trainingschedule_overview_index'));
    }
}
