<?php

namespace TrainingScheduleBundle\Controller;

use DateTime;
use TrainingScheduleBundle\Entity\StatisticEntry;
use TrainingScheduleBundle\Entity\TrainingDay;
use TrainingScheduleBundle\Form\StatisticEntryContainerType;
use TrainingScheduleBundle\Form\StatisticEntryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use TrainingScheduleBundle\Repository\TrainingDayRepository;
use TrainingScheduleBundle\Security\AccessAttributes;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class StatisticEntryController
 *
 * @package TrainingScheduleBundle\Controller
 *
 * RoutePrefix statistic_entry
 */
class StatisticEntryController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        return $this->render(
            'TrainingScheduleBundle:StatisticEntry:index.html.twig',
            array()
        );
    }

    /**
     * @Route("/new/{date}")
     * @ParamConverter("date", options={"format": "d-m-Y"})
     */
    public function newAction(Request $request, DateTime $date)
    {
        $form = $this->createForm(
            StatisticEntryContainerType::class,
            null,
            array(
                'data' => array('date' => $date),
            )
        );
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                /** @var UsernamePasswordToken $token */
                $token = $this->container->get('security.token_storage')->getToken();
                $data = $form->getData();
                /** @var TrainingDayRepository $repository */
                $repository = $this->getDoctrine()->getManager()->getRepository('TrainingScheduleBundle:TrainingDay');
                $trainingDay = $repository->findOneBy(array('date' => $data['date'], 'user' => $token->getUser()));
                if (is_null($trainingDay)) {
                    $trainingDay = new TrainingDay();
                    $trainingDay->setDate($data['date']);
                    $trainingDay->setUser($token->getUser());
                    $this->getDoctrine()->getManager()->persist($trainingDay);
                }
                /** @var StatisticEntry $statEntry */
                $statEntry = $data['statisticEntry'];
                $statEntry->setTrainingDay($trainingDay);

                $this->getDoctrine()->getManager()->persist($trainingDay);
                $this->getDoctrine()->getManager()->persist($trainingDay);
                $this->getDoctrine()->getManager()->persist($data['statisticEntry']);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('trainingschedule_overview_index');
            }
        }

        return $this->render(
            'TrainingScheduleBundle:StatisticEntry:new.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/view/{id}")
     * @ParamConverter("statisticEntry", class="TrainingScheduleBundle:StatisticEntry")
     * @param StatisticEntry $statisticEntry
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(StatisticEntry $statisticEntry)
    {

        $form = $this->createForm(
            StatisticEntryType::class,
            $statisticEntry,
            array(
                'disabled' => true,
            )
        );

        return $this->render(
            'TrainingScheduleBundle:StatisticEntry:view.html.twig',
            array(
                'form' => $form->createView(),
                'id'   => $statisticEntry->getId()
            )
        );
    }

    /**
     * @Route("/edit/{id}")
     * @ParamConverter("statisticEntry", class="TrainingScheduleBundle:StatisticEntry")
     * @param StatisticEntry $statisticEntry
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, StatisticEntry $statisticEntry)
    {
        $form = $this->createForm(
            StatisticEntryType::class,
            $statisticEntry,
            array()
        );

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($statisticEntry);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('trainingschedule_overview_index');
            }
        }

        return $this->render(
            'TrainingScheduleBundle:StatisticEntry:edit.html.twig',
            array(
                'form' => $form->createView(),
                'id'   => $statisticEntry->getId(),
            )
        );
    }

    /**
     * @Route("/delete/{id}")
     * @ParamConverter("statisticEntry", class="TrainingScheduleBundle:StatisticEntry")
     */
    public function deleteAction(StatisticEntry $statisticEntry)
    {
        $this->getDoctrine()->getManager()->remove($statisticEntry);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('trainingschedule_overview_index');

    }

}
