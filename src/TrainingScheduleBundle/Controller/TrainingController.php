<?php

namespace TrainingScheduleBundle\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use TrainingScheduleBundle\Entity\EnduranceTraining;
use TrainingScheduleBundle\Entity\StatisticEntry;
use TrainingScheduleBundle\Entity\StrengthTraining;
use TrainingScheduleBundle\Entity\Training;
use TrainingScheduleBundle\Entity\TrainingDay;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use TrainingScheduleBundle\Entity\User;
use TrainingScheduleBundle\Form\ContainerType\TrainingContainerType;
use TrainingScheduleBundle\Helper\TrainingHelper;
use TrainingScheduleBundle\Repository\TrainingDayRepository;

/**
 * Class TrainingController
 *
 * @package TrainingScheduleBundle\Controller
 *
 * RoutePrefix training
 */
class TrainingController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        return $this->render(
            'TrainingScheduleBundle:Training:index.html.twig',
            array(// ...
            )
        );
    }

    /**
     * @Route("/edit/{id}")
     * @ParamConverter("training", class="TrainingScheduleBundle:Training")
     * @param Training $training
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Training $training)
    {
        if ($training instanceof EnduranceTraining) {
            return $this->forward("TrainingScheduleBundle:EnduranceTraining:edit", array('id' => $training->getId()));
        }
        if ($training instanceof StrengthTraining) {
            return $this->forward("TrainingScheduleBundle:StrengthTraining:edit", array('id' => $training->getId()));
        }
    }

    /**
     * @Route("/new/{date}")
     * @ParamConverter("date", options={"format": "d-m-Y"})
     * @param Request  $request
     * @param Datetime $date
     *
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function newAction(Request $request, Datetime $date)
    {

        $form = $this->createForm(
            TrainingContainerType::class,
            null,
            array(
                'data'     => array(
                    'date'       => $date,
                    'route_back' => $request->getHost().$this->generateUrl('trainingschedule_overview_index', array(), UrlGeneratorInterface::ABSOLUTE_PATH),
                ),
                'required' => false,
            )
        );
        /** @var UsernamePasswordToken $token */
        $token = $this->container->get('security.token_storage')->getToken();
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                /** @var TrainingDayRepository $repository */
                $repository = $this->getDoctrine()->getManager()->getRepository('TrainingScheduleBundle:TrainingDay');
                $trainingDay = $repository->findOneBy(array('date' => $data['date'], 'user' => $token->getUser()));
                if (is_null($trainingDay)) {
                    $trainingDay = new TrainingDay();
                    $trainingDay->setDate($data['date']);
                    $trainingDay->setUser($token->getUser());
                    $this->getDoctrine()->getManager()->persist($trainingDay);
                }
                if ($data['Training']['type'] == EnduranceTraining::__className()) {
                    /** @var EnduranceTraining $enduranceTraining */
                    $enduranceTraining = $data['Training']['enduranceTraining'];
                    $enduranceTraining->setName($data['Training']['name']);
                    $enduranceTraining->setNote($data['Training']['note']);
                    $enduranceTraining->setTrainingDay($trainingDay);
                    $this->getDoctrine()->getManager()->persist($enduranceTraining);
                }
                if ($data['Training']['type'] == StrengthTraining::__className()) {
                    /** @var StrengthTraining $strengthTraining */
                    $strengthTraining = $data['Training']['strengthTraining'];
                    $strengthTraining->setName($data['Training']['name']);
                    $strengthTraining->setNote($data['Training']['note']);
                    $strengthTraining->setTrainingDay($trainingDay);
                    $this->getDoctrine()->getManager()->persist($strengthTraining);
                }
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('trainingschedule_overview_index');
            }

        }

        // generate suggestion for last weekday
        $weekDaySuggestions = TrainingHelper::generateWeekDaySuggestion($this->getDoctrine()->getManager(), $date, $token->getUser());

        // generate suggestion for last entries
        $ownTrainingSuggestions = TrainingHelper::generateOwnTrainingSuggestion($this->getDoctrine()->getManager(), $date, $token->getUser());


        return $this->render(
            'TrainingScheduleBundle:Training:new.html.twig',
            array(
                'form'                   => $form->createView(),
                'weekDaySuggestions'     => $weekDaySuggestions,
                'ownTrainingSuggestions' => $ownTrainingSuggestions,
            )
        );
    }

    /**
     * @Route("/delete/{id}")
     * @ParamConverter("training", class="TrainingScheduleBundle:Training")
     * @param Training $training
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Training $training)
    {
        if ($training instanceof EnduranceTraining) {
            return $this->forward("TrainingScheduleBundle:EnduranceTraining:delete", array('id' => $training->getId()));
        }
        if ($training instanceof StrengthTraining) {
            return $this->forward("TrainingScheduleBundle:StrengthTraining:delete", array('id' => $training->getId()));
        }
    }

    /**
     * @Route("/view/{id}")
     * @ParamConverter("training", class="TrainingScheduleBundle:Training")
     * @param Training $training
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Training $training)
    {
        if ($training instanceof EnduranceTraining) {
            return $this->forward("TrainingScheduleBundle:EnduranceTraining:view", array('id' => $training->getId()));
        }
        if ($training instanceof StrengthTraining) {
            return $this->forward("TrainingScheduleBundle:StrengthTraining:view", array('id' => $training->getId()));
        }
    }
}
