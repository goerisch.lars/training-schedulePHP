<?php

namespace TrainingScheduleBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Ob\HighchartsBundle\Highcharts\Highchart;
use TrainingScheduleBundle\Helper\ProfileHelper;

class ProfileController extends Controller
{
    /**
     * Show the user
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $chartHelper = $this->get('profile_helper');
        /** @var Highchart $chart */
        $chartMonth = $chartHelper->getMonthWeightGraph();

        $chartWeek = $chartHelper->getWeekWeightGraph();

        return $this->render(
            'FOSUserBundle:Profile:show.html.twig',
            array(
                'user'       => $user,
                'chartMonth' => $chartMonth,
                'chartWeek'  => $chartWeek,
            )
        );
    }

}
