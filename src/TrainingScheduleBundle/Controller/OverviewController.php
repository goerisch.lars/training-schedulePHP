<?php

namespace TrainingScheduleBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use TrainingScheduleBundle\Helper\OverviewHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TrainingScheduleBundle\Repository\TrainingDayRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class OverviewController
 *
 * @package TrainingScheduleBundle\Controller
 *
 * RoutePrefix overview
 */
class OverviewController extends Controller
{
    /**
     * @Route("/index")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        // get user from session context not implemented yet
        /** @var User $user */
        // $user = $this->get('security.token_storage')->getToken()->getUser();

        $date = new \DateTime();
        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();
        /** @var TrainingDayRepository $trainingDayRepository */
        $trainingDayRepository = $manager->getRepository('TrainingScheduleBundle:TrainingDay');

        // prepare for submitting
        if ($request->isMethod(Request::METHOD_POST)) {
            $date = new \DateTime($request->get('date'));
        }

        $helper = new OverviewHelper($this->get('translator'));
        $dates = $helper->calculateWeekDates($date);
        $weekdays = $helper->getWeekNames();
        /** @var UsernamePasswordToken $token */
        $token = $this->container->get('security.token_storage')->getToken();
        $trainingDays = array();
        foreach ($dates as $date) {
            // add user later
            $trainingDays[] = $trainingDayRepository->findOneBy(array('date' => $date, 'user' => $token->getUser()));
        }

        return $this->render(
            'TrainingScheduleBundle:Overview:index.html.twig',
            array(
                'dates'        => $dates,
                'weekdays'     => $weekdays,
                'trainingDays' => $trainingDays,
            )
        );
    }

}
