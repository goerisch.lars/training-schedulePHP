<?php

namespace TrainingScheduleBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;

/**
 * Class SecurityController
 *
 * @package TrainingScheduleBundle\Controller
 */
class SecurityController extends BaseSecurityController
{
    public function loginAction(Request $request)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl('trainingschedule_overview_index'));
        }

        return parent::loginAction($request);
    }
}
