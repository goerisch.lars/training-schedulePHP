<?php

namespace TrainingScheduleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use TrainingScheduleBundle\Entity\EnduranceTraining;
use TrainingScheduleBundle\Entity\Training;
use TrainingScheduleBundle\Form\InheritanceType\EnduranceTrainingType;
use TrainingScheduleBundle\Security\AccessAttributes;
use TrainingScheduleBundle\Tests\Controller\EnduranceTrainingControllerTest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class EnduranceTrainingController
 *
 * @package TrainingScheduleBundle\Controller
 *
 * RoutePrefix endurance_training
 */
class EnduranceTrainingController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        return $this->render(
            'TrainingScheduleBundle:EnduranceTraining:index.html.twig',
            array(// ...
            )
        );
    }

    /**
     * @Route("/new")
     */
    public function newAction()
    {
        $form = $this->createForm(EnduranceTrainingType::class);

        return $this->render(
            'TrainingScheduleBundle:EnduranceTraining:new.html.twig',
            array(
                'form' => $form->createView(),
                // ...
            )
        );
    }

    /**
     * @Route("/view/{id}")
     * @ParamConverter("enduranceTraining", class="TrainingScheduleBundle:EnduranceTraining")
     * @param EnduranceTraining $enduranceTraining
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(EnduranceTraining $enduranceTraining)
    {
        $this->denyAccessUnlessGranted(AccessAttributes::VIEW, $enduranceTraining);

        $form = $this->createForm(
            EnduranceTrainingType::class,
            $enduranceTraining,
            array(
                'disabled' => true,
            )
        );

        return $this->render(
            'TrainingScheduleBundle:Training:view.html.twig',
            array(
                'form' => $form->createView(),
                'type' => $enduranceTraining::__className(),
                'id'   => $enduranceTraining->getId(),
            )
        );
    }

    /**
     * @Route("/edit/{id}")
     * @ParamConverter("enduranceTraining", class="TrainingScheduleBundle:EnduranceTraining")
     * @param Request           $request
     * @param EnduranceTraining $enduranceTraining
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, EnduranceTraining $enduranceTraining)
    {
        $this->denyAccessUnlessGranted(AccessAttributes::VIEW, $enduranceTraining);

        $form = $this->createForm(EnduranceTrainingType::class, $enduranceTraining, array());

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($enduranceTraining);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('trainingschedule_overview_index');
            }
        }

        return $this->render(
            'TrainingScheduleBundle:EnduranceTraining:edit.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/delete/{id}")
     * @ParamConverter("enduranceTraining", class="TrainingScheduleBundle:EnduranceTraining")
     * @param EnduranceTraining $enduranceTraining
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(EnduranceTraining $enduranceTraining)
    {
        $this->getDoctrine()->getManager()->remove($enduranceTraining);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('trainingschedule_overview_index');
    }

}
