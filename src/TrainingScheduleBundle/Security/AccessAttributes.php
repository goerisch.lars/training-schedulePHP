<?php

namespace TrainingScheduleBundle\Security;

/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 13.01.2016
 * Time: 15:07
 */
class AccessAttributes
{
    const VIEW = 'VIEW';
    const EDIT = 'EDIT';
    const DELETE = 'DELETE';
}