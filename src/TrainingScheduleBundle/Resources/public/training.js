/**
 * Created by Lars on 06.01.2016.
 */
$(document).ready(function () {
    $("#training_container_Training_type").change(function () {
        var selected = $("#training_container_Training_type option:selected").val();


        if (selected === 'StrengthTraining') {
            $('.StrengthTraining').each(function (i, obj) {
                $(obj).show();

            });
            $('.EnduranceTraining').each(function (i, obj) {
                $(obj).hide();

            });
        }
        if (selected === 'EnduranceTraining') {
            $('.EnduranceTraining').each(function (i, obj) {
                $(obj).show();

            });
            $('.StrengthTraining').each(function (i, obj) {
                $(obj).hide();

            });
        }
    });

    /*
     handle input on click in training new - copy values over to form
     */
    $('.suggestions').on('click', '.list-group-item', function (e) {

        var div = this;
        var classList = $(this).attr('class').split(/\s+/);
        $.each(classList, function (index, item) {
            if (item === 'StrengthTraining') {
                //noinspection JSDuplicatedDeclaration
                var attributes = ['name', 'note', 'rerun', 'sets', 'weight'];
                var values = extractValues(div, attributes);
                $("#training_container_Training_type").val("StrengthTraining");   // select from drop down list
                $("#training_container_Training_type").trigger("change");

                setTrainingFields(values);
            }

            if (item === 'EnduranceTraining') {
                //noinspection JSDuplicatedDeclaration
                var attributes = ['name', 'note', 'length'];
                var values = extractValues(div, attributes);
                $("#training_container_Training_type").val("EnduranceTraining");
                $("#training_container_Training_type").trigger("change");

                setTrainingFields(values);
            }
        });
    });

    function trainingSetName(name) {
        $('#training_container_Training_name').val(name)
    }

    function trainingSetNote(note) {
        $('#training_container_Training_note').val(note)
    }

    function trainingSetReruns(reruns) {
        $('#training_container_Training_strengthTraining_reruns').val(reruns)
    }

    function trainingSetSets(sets) {
        $('#training_container_Training_strengthTraining_sets').val(sets)
    }

    function trainingSetWeight(weight) {
        $('#training_container_Training_strengthTraining_weight').val(weight)
    }

    function trainingSetLength(length) {
        $('#training_container_Training_enduranceTraining_length').val(length)
    }

    function extractValues(obj, attributes) {
        var values = {};
        $.each($(obj).children().children(), function (index, item) {
            var element = this;
            $.each(attributes, function (index, item) {
                if ($(element).data(attributes[index]) != undefined) {
                    values[attributes[index]] = $(element).data(attributes[index]);
                }
            });
        });

        return values;
    }

    function setTrainingFields(valuesArray) {
        $.each(valuesArray, function (index, item) {
            switch (index) {
                case 'name' :
                    trainingSetName(item);
                    break;
                case 'note' :
                    trainingSetNote(item);
                    break;
                case 'rerun' :
                    trainingSetReruns(item);
                    break;
                case 'sets' :
                    trainingSetSets(item);
                    break;
                case 'weight' :
                    trainingSetWeight(item);
                    break;
                case 'length' :
                    trainingSetLength(item);
                    break;
            }
        });
    }
});
