/**
 * Created by Lars on 27.01.2016.
 */
$(document).ready(function () {

    $('#dateBackward').click(function (e) {
        e.stopPropagation();
        var date = new Date($('#date').val());
        date.setDate(date.getDate() - 7);
        $('#date').val(date.toISOString().slice(0, 10));
    });

    $('#dateForward').click(function (e) {
        e.stopPropagation();
        var date = new Date($('#date').val());
        date.setDate(date.getDate() + 7);
        $('#date').val(date.toISOString().slice(0, 10));
    });

});