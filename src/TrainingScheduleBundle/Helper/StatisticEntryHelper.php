<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 02.02.2016
 * Time: 23:05
 */

namespace TrainingScheduleBundle\Helper;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\LazyCriteriaCollection;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use TrainingScheduleBundle\Entity\StatisticEntry;
use TrainingScheduleBundle\Entity\Training;
use TrainingScheduleBundle\Entity\TrainingDay;
use TrainingScheduleBundle\Entity\User;
use TrainingScheduleBundle\Repository\TrainingDayRepository;

class StatisticEntryHelper
{

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var UsernamePasswordToken
     */
    private $userToken;

    /**
     * StatisticEntryHelper constructor.
     *
     * @param EntityManager $manager
     * @param TokenStorage  $tokenStorage
     */
    public function __construct(EntityManager $manager, TokenStorage $tokenStorage)
    {
        $this->manager = $manager;
        $this->userToken = $tokenStorage->getToken();
    }

    /**
     *
     * @param \DateTime $startDate beginning >=
     * @param \DateTime $endDate   end <=
     * @param           $type      - type of the StatisticEntryFilter
     *
     * @return StatisticEntry[]
     *
     * Note: can be replaced with DQL if performance issues are notable
     */
    public function getStatisticForRangeAndType(\DateTime $startDate, \DateTime $endDate, $type = null)
    {
        /** @var TrainingDayRepository $trainingDayRepository */
        $trainingDayRepository = $this->manager->getRepository('TrainingScheduleBundle:TrainingDay');

        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->where($expr->gte('date', $startDate));
        $criteria->andWhere($expr->lte('date', $endDate));
        $criteria->andWhere($expr->eq('user', $this->userToken->getUser()));

        /** @var LazyCriteriaCollection $trainingDays */
        $trainingDays = $trainingDayRepository->matching($criteria);

        $result = array();
        /** @var TrainingDay $trainingDay * */
        foreach ($trainingDays as $trainingDay) {
            foreach ($trainingDay->getStatistics() as $statistic) {
                /** @var StatisticEntry $statistic */
                if ($type != null) {
                    if ($statistic->getName() === $type) {
                        $result[] = $statistic;
                    }
                } else {
                    $result[] = $statistic;
                }

            }
        }

        return $result;
    }

    /**
     * @param StatisticEntry[] $statistics
     *
     * @return array
     */
    public function extractValues(array $statistics)
    {
        $result = array();
        /** @var StatisticEntry $statistic */
        foreach ($statistics as $statistic) {
            $result[] = $statistic->getValue();
        }

        return $result;
    }


    public function calculateMonths(\DateTime $startDate, \DateTime $endDate, $type, $monthSeparator = null)
    {
        $months = array();

        $tmpStart = new \DateTime($startDate->format('Y-m-d'));
        $tmpEnd = new \DateTime($endDate->format('Y-m-d'));
        $tmpEnd->modify('last day of this month');


        $end = false;
        while ($end == false) {
            $tmpMonthStart = new \DateTime($tmpStart->modify('first day of this month')->format('Y-m-d'));
            $tmpMonthEnd = (new \DateTime($tmpMonthStart->format('Y-m-d')))->modify('last day of this month');

            $months[$tmpMonthStart->format('M')] = $this->getStatisticForRangeAndType($tmpMonthStart, $tmpMonthEnd, $type);


            $tmpStart->modify('first day of next month');
            if ($tmpStart->getTimestamp() > $tmpEnd->getTimestamp()) {
                $end = true;
            }
        }

        return $months;
    }

    public function calculateWeeks(\DateTime $startDate, \DateTime $endDate, $type)
    {
        $months = array();

        $tmpStart = new \DateTime($startDate->format('Y-m-d'));
        $tmpEnd = new \DateTime($endDate->format('Y-m-d'));
        $tmpEnd->modify('next sunday');

        if ($tmpStart->format('l') != "Monday") {
            $tmpStart = $tmpStart->modify('last monday');
        }

        $end = false;
        while ($end == false) {
            $tmpMonthStart = new \DateTime($tmpStart->modify('monday this week')->format('Y-m-d'));
            $tmpMonthEnd = (new \DateTime($tmpMonthStart->format('Y-m-d')))->modify('sunday this week');

            $months[$tmpMonthStart->format('W')] = $this->getStatisticForRangeAndType($tmpMonthStart, $tmpMonthEnd, $type);


            $tmpStart->modify('monday next week');
            if ($tmpStart->getTimestamp() > $tmpEnd->getTimestamp()) {
                $end = true;
            }
        }

        return $months;
    }

    /**
     * @param array $months
     *
     * @return array
     */
    public function calculateAverage(array $values)
    {

        $result = array();

        foreach ($values as $key => $value) {
            $tmpValues = 0;
            /** @var StatisticEntry $statistics */
            foreach ($value as $statistics) {
                $tmpValues += $statistics->getValue();
            }
            if (count($value) > 0) {
                $result[$key] = $tmpValues / count($value);
            } else {
                $result[$key] = $tmpValues;
            }

        }

        return $result;
    }

    /**
     * @param array $average
     *
     * @return array
     */
    public function getHighAndLow(array $average)
    {
        $values = array_values($average);
        $low = min($values);
        $high = max($values);

        return array($low, $high);
    }
}