<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 20.01.2016
 * Time: 14:42
 */

namespace TrainingScheduleBundle\Helper;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\LazyCriteriaCollection;
use TrainingScheduleBundle\Entity\Training;
use TrainingScheduleBundle\Entity\TrainingDay;
use TrainingScheduleBundle\Entity\User;
use TrainingScheduleBundle\Repository\TrainingDayRepository;

class TrainingHelper
{
    /**
     * generates the suggestion entries for an given date and user
     *
     * @param EntityManager $manager
     * @param \DateTime     $currentDate
     * @param User          $user
     *
     * @return array|\TrainingScheduleBundle\Entity\Training[]
     */
    public static function generateWeekDaySuggestion(EntityManager $manager, \DateTime $currentDate, User $user)
    {

        // last weekday is the given date minus 7 weekdays
        $currentDate = $currentDate->setTime(0, 0, 0);
        $currentTmp = new \DateTime($currentDate->format("d-m-Y"));
        $currentTmp = $currentTmp->modify('-7 day');

        // find the given trainingsDay if exist
        /** @var TrainingDayRepository $trainingDayRepository */
        $trainingDayRepository = $manager->getRepository('TrainingScheduleBundle:TrainingDay');
        /** @var TrainingDay $trainingDay */
        $trainingDay = $trainingDayRepository->findOneBy(array('date' => $currentTmp, 'user' => $user));
        if ($trainingDay == null) {
            return array();
        }
        /** @var Training[] $trainings */
        $result = array();
        foreach ($trainingDay->getTrainings() as $training) {
            if (count($result) <= 6) {
                $result[] = $training;
            }
        }

        return $result;
    }

    /**
     * generates the suggestion entries for an given date and user
     *
     * @param EntityManager $manager
     * @param \DateTime     $currentDate
     * @param User          $user
     *
     * @return array|\TrainingScheduleBundle\Entity\Training[]
     */
    public static function generateOwnTrainingSuggestion(EntityManager $manager, \DateTime $currentDate, User $user)
    {

        // last weekday is the given date minus 7 weekdays
        $currentDate = $currentDate->setTime(0, 0, 0);
        $currentTmp = new \DateTime($currentDate->format("d-m-Y"));
        $minusSeven = new \DateTime($currentDate->format("d-m-Y"));
        $minusSeven = $minusSeven->modify('-7 day');

        // find the given trainingsDay if exist
        /** @var TrainingDayRepository $trainingDayRepository */
        $trainingDayRepository = $manager->getRepository('TrainingScheduleBundle:TrainingDay');

        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->where($expr->gte('date', $minusSeven));
        $criteria->andWhere($expr->lte('date', $currentTmp));
        $criteria->andWhere($expr->eq('user', $user));

        /** @var LazyCriteriaCollection $trainingDays */
        $trainingDays = $trainingDayRepository->matching($criteria);
        count($trainingDays);
        if ($trainingDays == null) {
            return array();
        }
        $result = array();
        foreach ($trainingDays as $trainingDay) {
            foreach ($trainingDay->getTrainings() as $training) {
                if (count($result) <= 6) {
                    $result[] = $training;
                }
            }
        }

        /** @var Training[] $trainings */
        return $result;
    }

}