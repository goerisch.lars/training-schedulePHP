<?php

namespace TrainingScheduleBundle\Helper;

use Symfony\Component\Templating\Helper\Helper;
use Symfony\Component\Translation\DataCollectorTranslator;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;
use TrainingScheduleBundle\Entity\StatisticEntry;
use Zend\Json\Expr;

/**
 * Class ProfileHelper
 */
class ProfileHelper extends Helper
{
    /**
     * @var DataCollectorTranslator
     */
    private $translator;

    /**
     * @var StatisticEntryHelper
     */
    private $statisticEntryHelper;

    public function __construct(TranslatorInterface $translator, StatisticEntryHelper $statisticEntryHelper)
    {
        $this->translator = $translator;
        $this->statisticEntryHelper = $statisticEntryHelper;
    }

    /**
     * @return Highchart Chart
     */
    public function getMonthWeightGraph()
    {

        $monthsWeight = $this->statisticEntryHelper->calculateMonths((new \DateTime())->modify('-1 year')->modify('first day of next month'), new \DateTime(), StatisticEntry::WEIGHT);
        $averageWeight = $this->statisticEntryHelper->calculateAverage($monthsWeight);
        $weightRange = $this->statisticEntryHelper->getHighAndLow($averageWeight);
        $roundedWeightRange = array(round($weightRange[0] - 5, -1) - 10, round($weightRange[1] + 5, -1));
        //ignore minus kg, not possible,  range are +- to 10, at 0 its rounded to -10
        $roundedWeightRange[0] < 0 ? $roundedWeightRange[0] = 0 : null;

        $monthsBodyFat = $this->statisticEntryHelper->calculateMonths((new \DateTime())->modify('-1 year')->modify('first day of next month'), new \DateTime(), StatisticEntry::BODY_FAT);
        $averageBodyFat = $this->statisticEntryHelper->calculateAverage($monthsBodyFat);
        $bodyFatRange = $this->statisticEntryHelper->getHighAndLow($averageBodyFat);

        $monthsMuscleMass = $this->statisticEntryHelper->calculateMonths((new \DateTime())->modify('-1 year')->modify('first day of next month'), new \DateTime(), StatisticEntry::MUSCLE_MASS);
        $averageMuscleMass = $this->statisticEntryHelper->calculateAverage($monthsMuscleMass);
        $muscleMassRange = $this->statisticEntryHelper->getHighAndLow($averageMuscleMass);
        $combinedRange = array_merge($bodyFatRange, $muscleMassRange);
        $combinedRange = $this->statisticEntryHelper->getHighAndLow($combinedRange);
        $roundedCombinedRange = array(round($combinedRange[0] - 5, -1), round($combinedRange[1] + 5, -1));
        //ignore minus %, not possible
        $roundedCombinedRange[0] < 0 ? $roundedCombinedRange[0] = 0 : null;

        $months = array();
        for ($i = 0; $i < 12; $i++) {
            $months[11 - $i] = $this->_trans(date("M", strtotime(date('Y-m-01')." -$i months")));
        }


        $chart = $this->_populateNewChart($averageWeight, $averageBodyFat, $averageMuscleMass, $roundedCombinedRange, $roundedWeightRange, $months);
        $chart->chart->renderTo('linechartMonthWeight'); // The #id of the div where to render the chart

        return $chart;
    }

    /**
     * @return Highchart
     */
    public function getWeekWeightGraph()
    {
        $weeksWeight = $this->statisticEntryHelper->calculateWeeks((new \DateTime())->modify('-1 year')->modify('sunday this week'), new \DateTime(), StatisticEntry::WEIGHT);
        $averageWeight = $this->statisticEntryHelper->calculateAverage($weeksWeight);
        $weightRange = $this->statisticEntryHelper->getHighAndLow($averageWeight);
        $roundedWeightRange = array(round($weightRange[0] - 5, -1) - 10, round($weightRange[1] + 5, -1));
        //ignore minus kg, not possible,  range are +- to 10, at 0 its rounded to -10
        $roundedWeightRange[0] < 0 ? $roundedWeightRange[0] = 0 : null;

        $weeksBodyFat = $this->statisticEntryHelper->calculateWeeks((new \DateTime())->modify('-1 year')->modify('sunday this week'), new \DateTime(), StatisticEntry::BODY_FAT);
        $averageBodyFat = $this->statisticEntryHelper->calculateAverage($weeksBodyFat);
        $bodyFatRange = $this->statisticEntryHelper->getHighAndLow($averageBodyFat);

        $weeksMuscleMass = $this->statisticEntryHelper->calculateWeeks((new \DateTime())->modify('-1 year')->modify('sunday this week'), new \DateTime(), StatisticEntry::MUSCLE_MASS);
        $averageMuscleMass = $this->statisticEntryHelper->calculateAverage($weeksMuscleMass);
        $muscleMassRange = $this->statisticEntryHelper->getHighAndLow($averageMuscleMass);
        $combinedRange = array_merge($bodyFatRange, $muscleMassRange);
        $combinedRange = $this->statisticEntryHelper->getHighAndLow($combinedRange);
        $roundedCombinedRange = array(round($combinedRange[0] - 5, -1), round($combinedRange[1] + 5, -1));
        //ignore minus %, not possible
        $roundedCombinedRange[0] < 0 ? $roundedCombinedRange[0] = 0 : null;

        $months = array();
        for ($i = 0; $i <= 52; $i++) {
            $months[52 - $i] = $this->_trans(date("W", strtotime(date('Y-m-01')." -$i weeks")));
        }

        $chart = $this->_populateNewChart($averageWeight, $averageBodyFat, $averageMuscleMass, $roundedCombinedRange, $roundedWeightRange, $months);
        $chart->chart->renderTo('linechartWeekWeight');

        return $chart;
    }

    /**
     * @param $averageWeight
     * @param $averageBodyFat
     * @param $averageMuscleMass
     * @param $roundedCombinedRange
     * @param $roundedWeightRange
     *
     * @return Highchart
     */
    private function _populateNewChart($averageWeight, $averageBodyFat, $averageMuscleMass, $roundedCombinedRange, $roundedWeightRange, $time)
    {
        $chart = new Highchart();
        $series = array(
            array(
                'name'  => $this->_trans(StatisticEntry::WEIGHT),
                'type'  => 'column',
                'color' => '#4572A7',
                'yAxis' => 1,
                'data'  => array_values($averageWeight),
            ),
            array(
                'name'  => $this->_trans(StatisticEntry::BODY_FAT),
                'type'  => 'spline',
                'color' => '#AA4643',
                'data'  => array_values($averageBodyFat),
            ),
            array(
                'name'  => $this->_trans(StatisticEntry::MUSCLE_MASS),
                'type'  => 'spline',
                'color' => '#FAB048',
                'data'  => array_values($averageMuscleMass),
            ),
        );

        $yData = array(
            array(
                'labels'       => array(
                    'formatter' => new Expr('function () { return this.value + " '.$this->_trans('Percent').'" }'),
                    'style'     => array('color' => '#AA4643'),
                ),
                'title'        => array(
                    'text'  => $this->_trans('Body Fat').' / '.$this->_trans('Muscle Mass'),
                    'style' => array('color' => '#AA4643'),
                ),
                'opposite'     => true,
                'min'          => $roundedCombinedRange[0],
                'max'          => $roundedCombinedRange[1],
                'maxPadding'   => 0,
                'minPadding'   => 0,
                'tickInterval' => 5,
                'tickAmount'   => (($roundedCombinedRange[1] - $roundedCombinedRange[0]) / 5) + 1,
            ),
            array(
                'labels'        => array(
                    'formatter' => new Expr('function () { return this.value + " kg" }'),
                    'style'     => array('color' => '#4572A7'),
                ),
                'title'         => array(
                    'text'  => $this->_trans(StatisticEntry::WEIGHT),
                    'style' => array('color' => '#4572A7'),
                ),
                'tickInterval'  => 5,
                'min'           => $roundedWeightRange[0],
                'tickAmount'    => (($roundedWeightRange[1] - $roundedWeightRange[0]) / 5) + 1, // ( (max - min) / tickInterval) + 1
                'endOnTick'     => false,
                'gridLineWidth' => 2,
                'max'           => $roundedWeightRange[1],
                'maxPadding'    => 0,
                'minPadding'    => 0,


            ),
        );
        $chart->yAxis($yData);
        $chart->chart->type('column');
        $chart->chart->backgroundColor('rgba(255, 255, 255, 0.7)');
        $chart->title->text($this->_trans('profile.chart.weight.headline'));
        $chart->xAxis->categories($time);
        $chart->legend->enabled(false);
        $formatter = new Expr(
            'function () {
                 var unit = {
                     "'.$this->_trans('Weight').'": " kg",
                     "'.$this->_trans('Body Fat').'": "'.$this->_trans('Body Fat').' %",
                     "'.$this->_trans('Muscle Mass').'": "'.$this->_trans('Muscle Mass').' %"
                 }[this.series.name];
                 return this.x + ": <b>" + this.y + "</b> " + unit;
             }'
        );
        $chart->tooltip->formatter($formatter);

        $chart->series($series);

        return $chart;
    }

    /** Translates a string
     *
     * @param $string
     *
     * @return string
     */
    private function _trans($string)
    {
        return $this->translator->trans($string, array(), 'TrainingScheduleBundle');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ProfileHelper';
    }
}