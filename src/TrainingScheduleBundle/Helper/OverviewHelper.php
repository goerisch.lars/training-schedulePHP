<?php

namespace TrainingScheduleBundle\Helper;

use Symfony\Component\Templating\Helper\Helper;
use Symfony\Component\Translation\DataCollectorTranslator;

/**
 * Class OverviewHelper
 */
class OverviewHelper extends Helper
{
    /**
     * @var DataCollectorTranslator
     */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }

    /**
     * returns an array with dates for the week,
     * given by the initial $date
     *
     * @param \DateTime $date
     *
     * @return array
     */
    public function calculateWeekDates(\DateTime $date)
    {
        if ($date->format('l') != "Monday") {
            $date = $date->modify('last monday');
        }
        $date = $date->setTime(0, 0, 0);
        $dates = array(
            0 => new \DateTime($date->format('Y-m-d')),
            1 => new \DateTime($date->modify('+1 day')->format('Y-m-d')),
            2 => new \DateTime($date->modify('+1 day')->format('Y-m-d')),
            3 => new \DateTime($date->modify('+1 day')->format('Y-m-d')),
            4 => new \DateTime($date->modify('+1 day')->format('Y-m-d')),
            5 => new \DateTime($date->modify('+1 day')->format('Y-m-d')),
            6 => new \DateTime($date->modify('+1 day')->format('Y-m-d')),
        );

        return $dates;
    }

    /**
     * @return array Translated week days in array
     */
    public function getWeekNames()
    {
        return array(
            0 => $this->translator->trans('weekdays.monday', array(), 'TrainingScheduleBundle'),
            1 => $this->translator->trans('weekdays.tuesday', array(), 'TrainingScheduleBundle'),
            2 => $this->translator->trans('weekdays.wednesday', array(), 'TrainingScheduleBundle'),
            3 => $this->translator->trans('weekdays.thursday', array(), 'TrainingScheduleBundle'),
            4 => $this->translator->trans('weekdays.friday', array(), 'TrainingScheduleBundle'),
            5 => $this->translator->trans('weekdays.saturday', array(), 'TrainingScheduleBundle'),
            6 => $this->translator->trans('weekdays.sunday', array(), 'TrainingScheduleBundle'),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'OverviewHelper';
    }
}