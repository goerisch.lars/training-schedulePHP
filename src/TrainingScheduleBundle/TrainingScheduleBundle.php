<?php

namespace TrainingScheduleBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TrainingScheduleBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
