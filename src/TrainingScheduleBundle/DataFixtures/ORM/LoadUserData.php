<?php
namespace TrainingScheduleBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TrainingScheduleBundle\Entity\User;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        /**
         * @var User $user1
         */
        $user1 = $userManager->createUser();
        $user1->setUsername('TestUser1');
        $user1->setEmail('email@test.com');
        $user1->setLocale('de');
        $user1->setEnabled(true);

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user1, '1234');
        $user1->setPassword($password);

        /**
         * @var User $user2
         */
        $user2 = $userManager->createUser();
        $user2->setUsername('TestUser2');
        $user2->setEmail('email@test2.com');
        $user2->setEnabled(true);
        $user2->setLocale('en');

        $password = $encoder->encodePassword($user2, '2345');
        $user2->setPassword($password);

        $userManager->updateUser($user1, true);
        $userManager->updateUser($user2, true);

        $this->addReference('user1', $user1);
        $this->addReference('user2', $user2);
    }

    public function getOrder()
    {
        return 1;
    }
}