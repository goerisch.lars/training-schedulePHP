<?php
namespace TrainingScheduleBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TrainingScheduleBundle\Entity\StatisticEntry;
use TrainingScheduleBundle\Entity\TrainingDay;

class CreateStatisticEntry extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getReference('user1');

        $currentDate = new \DateTime();
        $currentDate->modify('-10 days');

        for ($i = 0; $i < 55; $i++) {

            $day = new TrainingDay();
            $day->setDate(new \DateTime($currentDate->modify('-6 day')->format('Y-m-d')));
            $day->setUser($user);
            $manager->persist($day);

            $weightEntry = new StatisticEntry();
            $weightEntry->setName('Weight');
            $weightEntry->setValue(rand(70, 82));
            $weightEntry->setTrainingDay($day);

            $fatEntry = new StatisticEntry();
            $fatEntry->setName('Body Fat');
            $fatEntry->setValue(18 * (100 + $i) / 100);
            $fatEntry->setTrainingDay($day);

            $muscleEntry = new StatisticEntry();
            $muscleEntry->setName('Muscle Mass');
            $muscleEntry->setValue(10 * (100 - $i) / 100);
            $muscleEntry->setTrainingDay($day);

            $manager->persist($weightEntry);
            $manager->persist($fatEntry);
            $manager->persist($muscleEntry);
        }

        for ($i = 0; $i < 55; $i++) {

            $day = new TrainingDay();
            $day->setDate(new \DateTime($currentDate->modify('-6 day')->format('Y-m-d')));
            $day->setUser($user);
            $manager->persist($day);

            $weightEntry = new StatisticEntry();
            $weightEntry->setName('Weight');
            $weightEntry->setValue(rand(75, 82));
            $weightEntry->setTrainingDay($day);

            $manager->persist($weightEntry);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}