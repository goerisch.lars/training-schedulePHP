<?php
namespace TrainingScheduleBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TrainingScheduleBundle\Entity\EnduranceTraining;
use TrainingScheduleBundle\Entity\TrainingDay;


class CreateEnduranceTraining extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getReference('user1');

        $trainigDay = new TrainingDay();
        $trainigDay->setDate(new \DateTime());
        $trainigDay->setUser($user);

        $dateMonday = new \DateTime();
        $dateMonday->modify('last monday');

        $trainigMonday = new TrainingDay();
        $trainigMonday->setDate($dateMonday);
        $trainigMonday->setUser($user);

        $enduranceTraining = new EnduranceTraining();
        $enduranceTraining->setName('Joggen');
        $enduranceTraining->setLength(45);
        $enduranceTraining->setNote('Bli');

        $enduranceTraining2 = new EnduranceTraining();
        $enduranceTraining2->setName('Schwimmen');
        $enduranceTraining2->setLength(100);
        $enduranceTraining2->setNote('Bla');

        $enduranceTraining3 = new EnduranceTraining();
        $enduranceTraining3->setName('Laufen');
        $enduranceTraining3->setLength(150);
        $enduranceTraining3->setNote('Blub');

        $enduranceTraining->setTrainingDay($trainigDay);
        $enduranceTraining2->setTrainingDay($trainigDay);
        $enduranceTraining3->setTrainingDay($trainigMonday);

        $manager->persist($trainigDay);
        $manager->persist($trainigMonday);
        $manager->persist($enduranceTraining);
        $manager->persist($enduranceTraining2);
        $manager->persist($enduranceTraining3);
        $manager->flush();

        $this->addReference('trainingDay', $trainigDay);
        $this->addReference('trainingMonday', $trainigMonday);

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}