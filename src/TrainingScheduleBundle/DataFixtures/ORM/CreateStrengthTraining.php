<?php
namespace TrainingScheduleBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TrainingScheduleBundle\Entity\StrengthTraining;


class CreateStrengthTraining extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $trainigDay = $this->getReference('trainingDay');
        $trainigMonday = $this->getReference('trainingMonday');

        $strengthEntry1 = new StrengthTraining();
        $strengthEntry1->setName('Squats');
        $strengthEntry1->setNote('Bli');
        $strengthEntry1->setReruns(10);
        $strengthEntry1->setWeight(30);
        $strengthEntry1->setSets(3);

        $strengthEntry2 = new StrengthTraining();
        $strengthEntry2->setName('Beinpresse');
        $strengthEntry2->setNote('Bla');
        $strengthEntry2->setReruns(15);
        $strengthEntry2->setWeight(60);
        $strengthEntry2->setSets(3);

        $strengthEntry3 = new StrengthTraining();
        $strengthEntry3->setName('Kreuzheben');
        $strengthEntry3->setNote('Blub');
        $strengthEntry3->setReruns(8);
        $strengthEntry3->setWeight(20);
        $strengthEntry3->setSets(4);


        $strengthEntry1->setTrainingDay($trainigDay);
        $strengthEntry2->setTrainingDay($trainigMonday);
        $strengthEntry3->setTrainingDay($trainigMonday);

        $manager->persist($strengthEntry1);
        $manager->persist($strengthEntry2);
        $manager->persist($strengthEntry3);
        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}