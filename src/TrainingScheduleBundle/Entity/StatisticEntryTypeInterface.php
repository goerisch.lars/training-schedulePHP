<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 03.02.2016
 * Time: 00:21
 */

namespace TrainingScheduleBundle\Entity;


interface StatisticEntryTypeInterface
{
    const WEIGHT = 'Weight';
    const CHEST_GIRTH = 'Chest Girth';
    const ABDOMINAL_GIRTH = 'Abdominal Girth';
    const HIP_MEASUREMENT = 'Hip Measurement';
    const BODY_FAT = 'Body Fat';
    const MUSCLE_MASS = 'Muscle Mass';
}