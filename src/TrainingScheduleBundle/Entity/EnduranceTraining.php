<?php

namespace TrainingScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnduranceTraining
 *
 * @ORM\Entity
 * @ORM\Table(name="endurance_training")
 * @ORM\Entity(repositoryClass="TrainingScheduleBundle\Repository\EnduranceTrainingRepository")
 */
class EnduranceTraining extends Training
{
    /**
     * @var int
     *
     * @ORM\Column(name="length", type="integer")
     */
    private $length;

    /**
     * Set length
     *
     * @param integer $length
     *
     * @return EnduranceTraining
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    public static function __className()
    {
        return 'EnduranceTraining';
    }
}

