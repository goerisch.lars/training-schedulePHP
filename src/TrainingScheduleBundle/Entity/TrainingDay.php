<?php

namespace TrainingScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrainingDay
 *
 * @ORM\Table(name="training_day")
 * @ORM\Entity(repositoryClass="TrainingScheduleBundle\Repository\TrainingDayRepository")
 */
class TrainingDay
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user__id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Training", mappedBy="trainingDay")
     */
    private $trainings;

    /**
     * @ORM\OneToMany(targetEntity="StatisticEntry", mappedBy="trainingDay")
     */
    private $statistics;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return TrainingDay
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Training[]
     */
    public function getTrainings()
    {
        return $this->trainings->getValues();
    }

    /**
     * @param mixed $trainings
     *
     * @return $this
     */
    public function setTrainings($trainings)
    {
        $this->trainings = $trainings;

        return $this;
    }

    /**
     * @return StatisticEntry[]
     */
    public function getStatistics()
    {
        return $this->statistics->getValues();
    }

    /**
     * @param mixed $statistics
     *
     * @return $this
     */
    public function setStatistics($statistics)
    {
        $this->statistics = $statistics;

        return $this;
    }
}

