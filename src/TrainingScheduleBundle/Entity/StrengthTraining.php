<?php

namespace TrainingScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ReflectionClass;

/**
 * StrengthTraining
 *
 * @ORM\Entity
 * @ORM\Table(name="strength_training")
 * @ORM\Entity(repositoryClass="TrainingScheduleBundle\Repository\StrengthTrainingRepository")
 */
class StrengthTraining extends Training
{

    /**
     * @var int
     *
     * @ORM\Column(name="reruns", type="integer")
     */
    private $reruns;

    /**
     * @var int
     *
     * @ORM\Column(name="sets", type="integer")
     */
    private $sets;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * Set reruns
     *
     * @param integer $reruns
     *
     * @return StrengthTraining
     */
    public function setReruns($reruns)
    {
        $this->reruns = $reruns;

        return $this;
    }

    /**
     * Get reruns
     *
     * @return int
     */
    public function getReruns()
    {
        return $this->reruns;
    }

    /**
     * Set sets
     *
     * @param integer $sets
     *
     * @return StrengthTraining
     */
    public function setSets($sets)
    {
        $this->sets = $sets;

        return $this;
    }

    /**
     * Get sets
     *
     * @return int
     */
    public function getSets()
    {
        return $this->sets;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     *
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    public static function __className()
    {
        return 'StrengthTraining';
    }


}

