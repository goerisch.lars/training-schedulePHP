<?php

namespace TrainingScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatisticEntry
 *
 * @ORM\Table(name="statistic_entry")
 * @ORM\Entity(repositoryClass="TrainingScheduleBundle\Repository\StatisticEntryRepository")
 */
class StatisticEntry implements StatisticEntryTypeInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingDay")
     * @ORM\JoinColumn(name="training_day_id", referencedColumnName="id")
     */
    private $trainingDay;


    public static $choices
        = array(
            StatisticEntryTypeInterface::WEIGHT          => StatisticEntryTypeInterface::WEIGHT,
            StatisticEntryTypeInterface::CHEST_GIRTH     => StatisticEntryTypeInterface::CHEST_GIRTH,
            StatisticEntryTypeInterface::ABDOMINAL_GIRTH => StatisticEntryTypeInterface::ABDOMINAL_GIRTH,
            StatisticEntryTypeInterface::HIP_MEASUREMENT => StatisticEntryTypeInterface::HIP_MEASUREMENT,
            StatisticEntryTypeInterface::BODY_FAT        => StatisticEntryTypeInterface::BODY_FAT,
            StatisticEntryTypeInterface::MUSCLE_MASS     => StatisticEntryTypeInterface::MUSCLE_MASS,
        );

    public static $unit
        = array(
            StatisticEntryTypeInterface::WEIGHT          => 'kg',
            StatisticEntryTypeInterface::CHEST_GIRTH     => 'cm',
            StatisticEntryTypeInterface::ABDOMINAL_GIRTH => 'cm',
            StatisticEntryTypeInterface::HIP_MEASUREMENT => 'cm',
            StatisticEntryTypeInterface::BODY_FAT        => '%',
            StatisticEntryTypeInterface::MUSCLE_MASS     => '%',
        );

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StatisticEntry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return StatisticEntry
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getTrainingDay()
    {
        return $this->trainingDay;
    }

    /**
     * @param mixed $trainingDay
     *
     * @return $this
     */
    public function setTrainingDay($trainingDay)
    {
        $this->trainingDay = $trainingDay;

        return $this;
    }

    public function getUnit()
    {
        return StatisticEntry::$unit[$this->getName()];
    }
}

