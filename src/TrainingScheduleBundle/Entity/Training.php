<?php

namespace TrainingScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Training
 *
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"enduranceTraining" = "EnduranceTraining", "strengthTraining" = "StrengthTraining"})
 */
abstract class Training
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255)
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingDay")
     * @ORM\JoinColumn(name="training_day_id", referencedColumnName="id")
     */
    private $trainingDay;

    private $endurance;

    private $strength;

    /**
     * @return mixed
     */
    public function getTrainingDay()
    {
        return $this->trainingDay;
    }

    /**
     * @param mixed $trainingDay
     *
     * @return $this
     */
    public function setTrainingDay($trainingDay)
    {
        $this->trainingDay = $trainingDay;

        return $this;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Training
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Training
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}

