<?php

namespace TrainingScheduleBundle\Form\ContainerType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use TrainingScheduleBundle\Form\TrainingType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TrainingContainerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'date',
            DateType::class,
            array(
                'data' => $options['data']['date'],
                'attr' => array('class' => 'form-control'),
            )
        )
            ->add('Training', TrainingType::class, array('label' => false))
            ->add(
                'back',
                ButtonType::class,
                array(
                    'label' => 'back',
                    'attr'  => array(
                        'class'   => 'btn btn-default pull-left',
                        'action'  => $options['data']['route_back'],
                        'onClick' => 'location.href ="http://'.$options['data']['route_back'].'"',
                    ),
                )
            )
            ->add(
                'new',
                SubmitType::class,
                array(
                    'label' => 'new',
                    'attr'  => array('class' => 'btn btn-primary pull-right'),
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'translation_domain' => 'TrainingScheduleBundle',
            )
        );
    }

}
