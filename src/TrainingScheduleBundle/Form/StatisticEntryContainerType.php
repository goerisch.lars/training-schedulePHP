<?php
/**
 * Created by PhpStorm.
 * User: Aska
 * Date: 06.01.2016
 * Time: 16:04
 */

namespace TrainingScheduleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StatisticEntryContainerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'date',
                DateType::class,
                array(
                    'data' => $options['data']['date'],
                    'attr' => array('class' => 'form-control'),
                )
            )
            ->add('statisticEntry', StatisticEntryType::class, array('label' => false))
            ->add(
                'new',
                SubmitType::class,
                array(
                    'label' => 'new',
                    'attr'  => array('class' => 'btn btn-default pull-right', 'style' => 'margin-top: 1%;'),
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'translation_domain' => 'TrainingScheduleBundle',
            )
        );
    }
}