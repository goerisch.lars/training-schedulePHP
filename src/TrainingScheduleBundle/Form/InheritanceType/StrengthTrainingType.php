<?php

namespace TrainingScheduleBundle\Form\InheritanceType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StrengthTrainingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('note', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('reruns', IntegerType::class, array('attr' => array('class' => 'form-control')))
            ->add('sets', IntegerType::class, array('attr' => array('class' => 'form-control')))
            ->add('weight', IntegerType::class, array('attr' => array('class' => 'form-control')));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'         => 'TrainingScheduleBundle\Entity\StrengthTraining',
                'translation_domain' => 'TrainingScheduleBundle',
            )
        );
    }
}
