<?php

namespace TrainingScheduleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TrainingScheduleBundle\Entity\EnduranceTraining;
use TrainingScheduleBundle\Entity\StrengthTraining;

/**
 * special case here, because of the option to chose 2 inheritance classes at once you must declare the form manually,
 * instead of automatic object mapping
 *
 * Class TrainingType
 *
 * @package TrainingScheduleBundle\Form
 */
class TrainingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tmpo = "dad";
        $tmpo = $tmpo;
        $builder
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add(
                'type',
                ChoiceType::class,
                array(
                    'choices' => array(
                        StrengthTraining::__className()  => 'StrengthTraining',
                        EnduranceTraining::__className() => 'EnduranceTraining',
                    ),
                    'data'    => StrengthTraining::__className(),
                    'attr'    => array('class' => 'form-control'),
                )
            )
            ->add('note', TextType::class, array('attr' => array('class' => 'form-control')))
            // enduranceTypes
            ->add('enduranceTraining', EnduranceTrainingType::class, array('label' => false, 'attr' => array('style' => 'display:none;', 'class' => 'EnduranceTraining')))
            // strengthTypes
            ->add('strengthTraining', StrengthTrainingType::class, array('label' => false, 'attr' => array('class' => 'StrengthTraining')));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'attr'               => array('class' => 'form-group'),
                'translation_domain' => 'TrainingScheduleBundle',
            )
        );
    }

}
