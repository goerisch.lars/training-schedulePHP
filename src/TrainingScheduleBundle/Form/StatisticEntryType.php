<?php

namespace TrainingScheduleBundle\Form;

use Proxies\__CG__\TrainingScheduleBundle\Entity\StatisticEntry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StatisticEntryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                ChoiceType::class,
                array(
                    'choices' => StatisticEntry::$choices,
                    'attr'    => array('class' => 'form-control'),
                )
            )
            ->add('value', IntegerType::class, array('attr' => array('class' => 'form-control')));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'         => 'TrainingScheduleBundle\Entity\StatisticEntry',
                'translation_domain' => 'TrainingScheduleBundle',
            )
        );
    }
}
