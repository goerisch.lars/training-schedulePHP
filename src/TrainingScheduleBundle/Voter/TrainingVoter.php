<?php

namespace TrainingScheduleBundle\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use TrainingScheduleBundle\Entity\User;
use TrainingScheduleBundle\Entity\Training;
use TrainingScheduleBundle\Security\AccessAttributes;

/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 13.01.2016
 * Time: 14:54
 */
class TrainingVoter extends Voter
{
    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed  $subject   The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     * @throws \Exception
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(AccessAttributes::VIEW, AccessAttributes::EDIT, AccessAttributes::DELETE))) {
            return false;
        }

        if (!$subject instanceof Training) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     *
     * @param string                                                               $attribute
     * @param mixed                                                                $subject
     * @param \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch ($attribute) {
            case AccessAttributes::VIEW:
                return $this->canView($subject, $user);
            case AccessAttributes::EDIT:
                return $this->canEdit($subject, $user);
            case AccessAttributes::DELETE:
                return $this->canDelete($subject, $user);
        }

        return false;
    }

    /**
     * @param Training $training
     * @param          $user
     *
     * @return bool
     */
    private function canView(Training $training, $user)
    {
        if ($this->canEdit($training, $user)) {
            return true;
        }

        return false;
    }

    /**
     * @param Training $training
     * @param          $user
     *
     * @return bool
     */
    private function canEdit($training, $user)
    {
        if ($training->getTrainingDay()->getUser() == $user) {
            return true;
        }

        return false;
    }

    private function canDelete($training, $user)
    {
        if ($this->canEdit($training, $user)) {
            return true;
        }

        return false;
    }
}